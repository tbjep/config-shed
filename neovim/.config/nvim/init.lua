vim.g.mapleader = " "

require('plugins')
require('theme')
require('keymap')
require('lsp')
require('settings')
require('markdown')
require('snippets')
require('completion')
