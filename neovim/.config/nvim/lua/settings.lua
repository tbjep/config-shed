vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.mouse = "a"
vim.opt.relativenumber = true
vim.opt.relativenumber = true
vim.opt_global.completeopt = {"menu", "menuone"}
vim.opt.sw = 0
vim.opt.ts = 4

require('colorizer').setup {
	'*',
	css = { css = true }
}

require('lsp').add_on_attach_function(function(_, bufnr)
	local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
	buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
end)

local job = require'plenary.job'
local worktree = require('git-worktree')

-- op = Operation.Switch, Operation.Create, Operation.Delete
-- metadata = table of useful values (structure dependent on op)
--      Switch
--          path = path you switched to
--          prev_path = previous worktree path
--      Create
--          path = path where worktree created
--          branch = branch name
--          upstream = upstream remote name
--      Delete
--          path = path where worktree deleted
worktree.on_tree_change(function(op, metadata)
	if op == worktree.Operations.Create then
		job:new{
			command = '../init-worktree',
			cwd = worktree.get_root() .. "/" .. metadata.path,
			on_exit = function(j, return_val)
				print(return_val)
				print(j:result())
			end,
		}:start()
	end
end)

require 'nvim-treesitter.configs'.setup {
	highlight = {
		enable = true
	},
	incremental_selection = {
		enable = true
	}
}

local ls = require "luasnip"
ls.config.setup {
	updateevents = 'TextChanged,TextChangedI',
	region_check_events = 'CursorMoved',
	delete_check_events = 'TextChanged',
}
