local ls = require('luasnip')
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node

function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local same = function(index)
	return f(function(args)
		return args[1]
	end, { index })
end

local wrap_snip = function(snippet, start, stop)
	return {t(start), snippet, t(stop)}
end

local wrap_snip_braces = function(snippet) return wrap_snip(snippet, "{", "}") end
local wrap_snip_brackets = function(snippet) return wrap_snip(snippet, "[", "]") end

local empty_or_input_choice = function(index, alternative)
	return c(index, {t "", alternative})
end

-- required: 1 if optional parameters should always be present, 0 if user should decide, -1 if optional paremeters should never show up
local tex_make_optional_parameters = function(pos, optional)
	local optional_parameter_snippet = wrap_snip_brackets(i(1))
	if optional then
		optional_parameter_snippet = empty_or_input_choice(pos, sn(nil, optional_parameter_snippet))
	end
	return optional_parameter_snippet
end

-- params: how many required parameters should be inserted. -1 specifies that user should decide how many. raw parameters can also be passed as an array.
local tex_make_required_parameters = function(pos, params)
	local required_param_snippets = {}
	if type(params) == "number" then
		local param_count = params
		params = {}
		for index = 1, param_count do
			table.insert(params, i(index))
		end
	end
	for index, val in ipairs(params) do
		TableConcat(required_param_snippets, wrap_snip_braces(val))
	end
	return sn(pos, required_param_snippets)
end

local tex_same_as_parameter = function(param_pos)
	return f(function(args)
		return args[1][1]
	end, { param_pos })
end

-- name: name of TeX macro
-- inc_opt_params: 1 if optional parameters should always be present, 0 if user should decide, -1 if optional paremeters should never show up
-- params: how many required parameters should be inserted. raw parameters can also be passed as an array.
-- dsa
local tex_macro = function(name, inc_opt_params, params)
	local opt_params = inc_opt_params >= 0 and tex_make_optional_parameters(2, inc_opt_params) or t ""
	local out = { t("\\" .. name), opt_params, tex_make_required_parameters(1, params), i(0) }
	--print(vim.inspect(out))
	return out
end

ls.add_snippets(nil, {
	tex = {
		s("dc", tex_macro("documentclass", 0, 1)),
		s("env", {t "\\begin{", i(1), t "}", i(2), t "\\end{", same(1), t "}", i(0)}),
		--s("env", {unpack(tex_macro("begin", 0, 1)), i(2), t "\\end{", t "bruh", t "}", i(0)}),
		s("up", tex_macro("usepackage", 0, 1)),
		s("mf", tex_macro("frac", -1, 2)),
		s("ind", tex_macro("index", -1, 1)),
	},
})
