local lspconfig = require('lspconfig')

local registered_on_attach_functions = {}

local default_on_attach = function(client, bufnr)
	for _, f in ipairs(registered_on_attach_functions) do
		f(client, bufnr)
	end
end

local handlers = {
	["textDocument/signatureHelp"] = vim.lsp.handlers.signature_help,
}

local server_defaults = {
	on_attach = default_on_attach,
	capabilities = require('cmp_nvim_lsp').default_capabilities(),
	handlers = handlers,
}

local servers = {
	clangd = {},
	cmake = {},
	rnix = {},
	lua_ls = {
		cmd = { "lua-language-server" }, -- works with lua-language-server-git on AUR
		settings = {
			Lua = {
				telemetry = { enable = false },
				runtime = {
					version = 'LuaJIT',
					path = vim.split(package.path .. ";lua/?.lua;lua/?/init.lua", ";"),
				},
				diagnostics = {
					-- Get the language server to recognize the `vim` global
					globals = {'vim'},
				},
				workspace = {
					library = vim.api.nvim_get_runtime_file("", true),
					checkThirdParty = false,
				}
			}
		}
	},
	texlab = {
		settings = {
			texlab = {
				build = {
					forwardSearchAfter = true,
					onSave = false
				},
				forwardSearch = {
					executable = "zathura",
					args = {"--synctex-forward", "%l:1:%f", "%p"}
				},
			},
		},
	},
	--rust_analyzer = {},
	pylsp = {},
	ts_ls = {},
	eslint = {},
	astro = {},
	svelte = {},
	html = {},
	cssls = {},
}

for server, config in pairs(servers) do
	config = vim.tbl_deep_extend("force", server_defaults, config)
	lspconfig[server].setup(config)
end

require('rust-tools').setup {
	server = vim.tbl_deep_extend("force", server_defaults, {
		["rust-analyzer"] = {
			procMacro = {
				enable = true
			},
		},
	}),
}

return {
	add_on_attach_function = function(f) table.insert(registered_on_attach_functions, f) end
}
