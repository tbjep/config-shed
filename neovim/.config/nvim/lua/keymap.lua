local defaults = { noremap=true, silent=true }

local set_keymap = vim.api.nvim_set_keymap
--local buf_keymap = vim.api.nvim_buf_set_keymap

set_keymap('n', '<leader>n', '<cmd>let @/ = ""<CR>', defaults)

set_keymap('i', '<Tab>', "luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>' ", {  silent = true, expr = true,})
set_keymap('i', '<S-Tab>', "<cmd>lua require'luasnip'.jump(-1)<Cr>", { noremap = true, silent = true, })

set_keymap('s', '<Tab>', "<cmd>lua require('luasnip').jump(1)<Cr>", { noremap = true, silent = true, })
set_keymap('s', '<S-Tab>', "<cmd>lua require('luasnip').jump(-1)<Cr>", { noremap = true, silent = true, })

set_keymap('i', '<C-E>', "luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'", {  silent = true, expr = true,})
set_keymap('s', '<C-E>', "luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'", {  silent = true, expr = true,})
set_keymap('n', '<leader>t', '<cmd>lua require"telescope".extensions.git_worktree.git_worktrees()<cr>', defaults)
set_keymap('n', '<leader>T', '<cmd>lua require"telescope".extensions.git_worktree.create_git_worktree()<cr>', defaults)
set_keymap('n', '<leader>T', '<cmd>lua require"telescope".extensions.git_worktree.create_git_worktree()<cr>', defaults)
vim.keymap.set('n', '<leader>ff', function () require"telescope.builtin".git_files({ recurse_submodules = true, show_untracked = false }) end, defaults)
set_keymap('n', '<leader>fF', '<cmd>lua require"telescope.builtin".find_files()<cr>', defaults)
set_keymap('n', '<leader>gl', '<cmd>lua require"telescope.builtin".git_commits()<cr>', defaults)
set_keymap('n', '<leader>gL', '<cmd>lua require"telescope.builtin".git_bcommits()<cr>', defaults)
set_keymap('n', '<leader>gb', '<cmd>lua require"telescope.builtin".git_branches()<cr>', defaults)

set_keymap('n', '<leader>h', '<cmd>lua require"harpoon.ui".toggle_quick_menu()<cr>', defaults)
set_keymap('n', '<leader>H', '<cmd>lua require"harpoon.cmd-ui".toggle_quick_menu()<cr>', defaults)
set_keymap('n', '<leader>b', '<cmd>lua require"harpoon.ui".nav_next()<cr>', defaults)
set_keymap('n', '<leader>B', '<cmd>lua require"harpoon.ui".nav_prev()<cr>', defaults)
for i = 1, 9, 1 do
	vim.keymap.set('n', '<leader>' .. i, function ()
		require'harpoon.ui'.nav_file(i)
	end,
	defaults)
end
set_keymap('n', '<leader>m', '<cmd>lua require"harpoon.mark".add_file()<cr>', defaults)
set_keymap('n', '<leader>s', '<cmd>lua require"harpoon.term".gotoTerminal(1)<cr>', defaults)
set_keymap('n', '<leader>c', '<cmd>lua require"harpoon.term".sendCommand(1, 1)<cr>', defaults)
set_keymap('n', '<leader>+', '<cmd>let @+ = @"<cr>', defaults)
for i = 1, 6, 1 do
	vim.keymap.set('n', '<leader><F' .. i .. '>', function ()
		require'harpoon.term'.gotoTerminal(i)
	end,
	defaults)
end
vim.keymap.set('n', '<F1>', '<nop>', defaults)


local lsp_keymaps = {}

local function set_lsp_keymap(mode, lhs, rhs, opts)
	table.insert(lsp_keymaps, {mode = mode, lhs = lhs, rhs = rhs, opts = opts})
end

set_lsp_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', defaults)
set_lsp_keymap('n', 'gd', '<Cmd>lua require"telescope.builtin".lsp_definitions()<CR>', defaults)
set_lsp_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', defaults)
set_lsp_keymap('n', 'gi', '<cmd>lua require"telescope.builtin".lsp_implementations()<CR>', defaults)
set_lsp_keymap('i', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', defaults)
set_lsp_keymap('n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<CR>', defaults)
set_lsp_keymap('n', 'gr', '<cmd>lua require"telescope.builtin".lsp_references()<CR>', defaults)
set_lsp_keymap('n', '<localleader>s', '<cmd>lua require(\'lspconfig\').clangd.switch_source_header(0)<CR>', defaults)
set_lsp_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', defaults)
set_lsp_keymap('n', '<leader>cf', '<cmd>lua vim.lsp.buf.formatting()<CR>', defaults)
set_lsp_keymap('n', '<leader>ls', '<cmd>lua require"telescope.builtin".lsp_document_symbols()<CR>', defaults)
set_lsp_keymap('n', '<leader>lS', '<cmd>lua require"telescope.builtin".lsp_dynamic_workspace_symbols()<CR>', defaults)
set_lsp_keymap('n', '<leader>ld', '<cmd>lua require"telescope.builtin".diagnostics({ bufnr = 0 })<CR>', defaults)
set_lsp_keymap('n', '<leader>lD', '<cmd>lua require"telescope.builtin".diagnostics({ bufnr = nil })<CR>', defaults)
set_lsp_keymap('n', '<leader>Ld', '<cmd>lua vim.diagnostic.setloclist()<CR>', defaults)
set_lsp_keymap('n', '<leader>d', '<cmd>lua vim.diagnostic.open_float()<CR>', defaults)

require('lsp').add_on_attach_function(function (_, bufnr)
	local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
	for _, km in ipairs(lsp_keymaps) do
		buf_set_keymap(km.mode, km.lhs, km.rhs, km.opts)
	end
end)

return {lsp_keymaps = lsp_keymaps}
