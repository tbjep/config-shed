vim.cmd "packadd packer.nvim"

require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'
	use 'baskerville/vim-sxhkdrc'
	use 'junegunn/goyo.vim'
	use 'neovim/nvim-lspconfig'
	use 'nvim-lua/plenary.nvim'
	use {
		'nvim-telescope/telescope.nvim',
		requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
	}
	use {
		'Th3Whit3Wolf/onebuddy',
		requires = {{ 'tjdevries/colorbuddy.nvim' }}
	}
	use 'norcalli/nvim-colorizer.lua'
	use 'L3MON4D3/LuaSnip'
	use 'ThePrimeagen/git-worktree.nvim'
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate'
	}
	use 'romgrk/nvim-treesitter-context'
	use 'nvim-treesitter/playground'
	use 'ThePrimeagen/harpoon'
	use 'hrsh7th/nvim-cmp'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'saadparwaiz1/cmp_luasnip'
	use 'tpope/vim-fugitive'
	use 'simrat39/rust-tools.nvim'
	use 'wuelnerdotexe/vim-astro'
	use 'mfussenegger/nvim-jdtls'
	use 'R-nvim/R.nvim'
	use 'R-nvim/cmp-r'
end)

require("telescope").load_extension("git_worktree")
