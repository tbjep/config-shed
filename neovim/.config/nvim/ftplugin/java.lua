local config = {
	cmd = {'/usr/bin/jdtls'},
	root_dir = vim.fs.dirname(vim.fs.find({'gradlew', '.git', 'mvnw'}, { upward = true })[1]),
}
require('jdtls').start_or_attach(config)

local bufnr = vim.api.nvim_get_current_buf()

local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
for _, km in ipairs(require'keymap'.lsp_keymaps) do
	buf_set_keymap(km.mode, km.lhs, km.rhs, km.opts)
end
