#!/usr/bin/env nix-shell
#! nix-shell -i runghc -p "ghc.withPackages (ps: [])"

module Main where

import System.Process
import Data.List (uncons)
import Data.Maybe (catMaybes, isJust, listToMaybe)
import Control.Arrow (second)
import Control.Monad (join, liftM)
import System.Exit

maybeReadCreateProcess = fmap (liftM ifSuccessful) <$> readCreateProcessWithExitCode
    where ifSuccessful (exit, out, _) = case exit of
                                            ExitSuccess -> Just out
                                            _ -> Nothing

refUpstreamShell = shell "git for-each-ref --format=\"%(refname:short) %(upstream:short)\""

refUpstreamPairs = do 
    out <- readCreateProcess refUpstreamShell ""
    return $ (map $ second listToMaybe) . catMaybes $ uncons . words <$> lines out

refUpstreamPairs' = catMaybes <$> fmap p <$> refUpstreamPairs
    where p (f,s) = case s of
                    Just u -> Just (f,u)
                    Nothing -> Nothing

offsetShell local remote = shell $ join ["git rev-list --left-right --count \"",local,"...",remote,"\" --"]

offset :: String -> String -> IO (Maybe (Int, Int))
offset local remote = do
                        out <- maybeReadCreateProcess (offsetShell local remote) ""
                        let extractPair [l, r] = (l,r)
                        return $ extractPair <$> fmap read <$> words <$> out

describeOffset local remote = do
    p <- offset local remote
    return $ formatStatus <$> p
    where formatStatus (lo,ro) = local ++ " is " ++ show lo ++ " commits ahead and " ++ show ro ++ " commits behind " ++ remote ++ "."

main = do
    ps <- refUpstreamPairs'
    unlines <$> catMaybes <$> mapM (uncurry describeOffset) ps >>= putStr
