export PATH="$HOME/.cargo/bin:$HOME/.local/bin:$PATH"
export TERMINAL=alacritty
export EDITOR=nvim
export LESS='-RS --mouse'
export SYSTEMD_LESS=$LESS
export XDG_CONFIG_HOME=$HOME/.config

export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
[[ -f "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh" ]] && . "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"

export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh"
