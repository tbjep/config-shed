#
# ~/.bashrc
#

# enable vi mode
set -o vi

# allow for control + l to clear the screen in vi-mode
bind -m vi C-l:clear-screen
bind -m vi-insert C-l:clear-screen

alias ls='ls --color=auto'
PS1='[\u@\h $([ -v IN_NIX_SHELL ] && echo "nix{$name} ")\W]\$ '

alias make='make -j$(nproc)'

#manpdf() {
#	man -Tps $@ | zathura -
#}

# Tried to make a keybind for running with sudo
#bind -x '"\C-^M":"sudo -k $READLINE_LINE"'

# Command for going up the directory tree
up() {
	if [ $# -eq 1 ]; then
		cd $(printf '../%.0s' $(eval "echo {1.."$(($1>1 ? $1 : 1))"}"))
	elif [ $# -eq 0 ]; then
		cd ../
	else
		echo "Usage:" ${FUNCNAME[0]} "<levels to go up>"
		return -1
	fi
}


source /usr/share/git/completion/git-completion.bash
source /usr/share/bash-completion/completions/bspc

[[ -f ~/.local/opt/vcpkg/scripts/vcpkg_completion.bash ]] && source ~/.local/opt/vcpkg/scripts/vcpkg_completion.bash

command -v thefuck &> /dev/null && eval "$(thefuck --alias)"

alias e=$EDITOR

PROMPT_COMMAND='printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

eval "$(direnv hook bash)"
